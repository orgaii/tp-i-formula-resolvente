extern printf

section .data

vector dq 25.0,0.0,1.0,0.0
length dd 4

msg db "El vector es:", 10,13,10,13,"[",0
format db " %f ,",0
end db " %f ]",10,13,0
n dq 2.0


section .text
global main
main:
    mov ebp, esp                    ;enter 0, 0
    
    push vector                     ;push vector memory direction
    push dword[length]              ;push length 
    mov edx, [n+4]
    push edx
    mov edx, [n]
    push edx                        ;push n
    call producto_rvf
    add esp, 16                     ;restore stack
    push msg
    call printf
    add esp, 4                      ;restore stack
    push vector                     ;push vector memory direction
    push dword[length]              ;push length 
    call loop_print
    add esp, 8                      ;restore stack
    xor eax, eax
    ret
    
    producto_rvf:
        push ebp
        mov ebp, esp                 ;enter 0, 0

        fld qword [ebp+8]            ;load n in fpu
        mov eax, [ebp+16]            ;move length to eax
        mov ebx, [ebp+20]            ;move vector memory direction to ebx
        _loop:
            cmp eax, 0               ;compare length in eax to 0 to leave
            jz _leave
            dec eax                  ;decrece length in eax
            fld qword[ebx]           ;load vector element in fpu
            fmul st0, st1            ;multiply element for n
            fstp qword[ebx]          ;save  multiply element
            add ebx, 8               ;move to next element
            jmp _loop        
        
        _leave:
            mov ebp,esp
            pop ebp                  ;leave 
     
            ret
            
     loop_print:
        push ebp
        mov ebp, esp                 ;enter 0, 0
        mov eax, [ebp+8]             ;move length to eax
        mov ebx, [ebp+12]            ;move vector memory direction to ebx
        _loop_:
            cmp eax, 0               ;compare length in eax to 0 to leave
            jz _leave_
            cmp eax, 1               ;compare length in eax to 1 to print last element
            jz last
            dec eax                  ;decrece length in eax
            push eax                 ;push eax value to save length
            push dword[ebx+4]
            push dword[ebx]          ;push vector element to print
            push format
            call printf
            add esp, 12              ;restore stack
            pop eax                  ;pop eax to restore value length
            add ebx, 8               ;move to next element
            
            jmp _loop_ 
            
        last:   
            dec eax                  ;decrece length in eax
            push eax                 ;push eax value to save length
            push dword[ebx+4]
            push dword[ebx]          ;push vector element to print
            push end
            call printf
            add esp, 12              ;restore stack
            pop eax                  ;pop eax to restore value length
            add ebx, 8               ;move to next element
            
            jmp _loop_    
        
        _leave_:
            mov ebp,esp
            pop ebp                  ;leave  
     
            ret
