#include <stdio.h>
#include <stdlib.h>


extern void resolute(double a, double b, double c);

int main(int argc, char *argv[]) {
    if (argc != 4) {
        printf("Parametros insuficientes o mayores que los necesarios\n");

        return 1;
    } else {
        double a = atof(argv[1]);
        double b = atof(argv[2]);
        double c = atof(argv[3]);
        resolute(a, b, c);
        return 0;
    }
}


