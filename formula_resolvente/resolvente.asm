extern printf

section .data

a dq 0.0
b dq 0.0
c dq 0.0

notb        dq 0.0
square_b    dq 0.0

cons4       dq 4.0
cons2       dq 2.0

numerator   dq 0.0
divisor    dq 0.0

resultPos   dq 0.0
resultNeg   dq 0.0

format1 db "Las raíces son: %f y %f" , 10,13,0
format2 db "Las valores son: a = %f , b = %f y c = %f" , 10,13,0

section .text
global resolute

resolute:
    push ebp
    mov ebp, esp                   ;enter 0, 0

    call extract_parameters    
    call print_variables
    call store_neg_b
    call store_divisor
    call store_b_square
    call store_numerator
    call calculate_result_pos
    call calculate_result_neg

    push dword [resultNeg+4]
    push dword [resultNeg]         ;push negative result to print
    push dword [resultPos+4]
    push dword [resultPos]         ;push positive result to print
    push format1
    call printf
    add esp, 20

    xor eax, eax
    mov ebp,esp
    pop ebp                        ;leave  

    ret

    extract_parameters:
        mov eax, [ebp + 8]
        mov ebx, [ebp + 12]        ;extract parameter "a" from the stack
        mov [a], eax                
        mov [a+4], ebx             ;save in "a"
        mov eax, [ebp + 16]
        mov ebx, [ebp + 20]        ;extract parameter "b" from the stack
        mov [b], eax
        mov [b+4], ebx             ;save in "b"
        mov eax, [ebp + 24]
        mov ebx, [ebp + 28]        ;extract parameter "c" from the stack
        mov [c], eax
        mov [c+4], ebx             ;save in "c"
     
        ret

    print_variables:
        push ebp
        mov ebp, esp               ;enter 0, 0
        push dword [c+4]
        push dword [c]             ;push "c" to print
        push dword [b+4]
        push dword [b]             ;push "b" to print
        push dword [a+4]
        push dword [a]             ;push "a" to print
        push format2               ;push format2 to print variables
        call printf
        add esp, 28
        mov ebp,esp
        pop ebp                    ;leave   
     
        ret

    store_neg_b:
        finit
        fld qword [b]              ;load b in fpu 
        fchs                       ;change sign
        fstp qword [notb]          ;store -b in notb

        ret
    
    store_divisor:
        finit
        fld qword [cons2]           ;load cons 2 in fpu
        fld qword [a]               ;load a in fpu  
        fmul st0, st1
        fstp qword [divisor]        ;store divisor

        ret
    
    store_b_square:
        finit
        fld qword [b]               ;load b in fpu
        fmul st0, st0               ;b squared
        fstp qword [square_b]       ;store b^2 in square_b

        ret
    
    store_numerator:
        finit
        fld qword [cons4]           ;load cons 4 in fpu
        fld qword [a]               ;load a in fpu
        fld qword [c]               ;load c in fpu
        fmul st0, st1
        fmul st0, st2
        fld qword [square_b]
        fsub st0, st1
        fsqrt
        fst qword [numerator]       ;store root square b^2 - 4*a*c in numerator

        ret

    calculate_result_pos:
        fld qword [notb]            ;load notb in fpu
        fadd st0,st1        
        fld qword [divisor]
        fdivr st0,st1
        fstp qword [resultPos]      ;store positive result

        ret

    calculate_result_neg:
        finit          
        fld qword [numerator]
        fld qword [notb]            ;load notb in fpu
        fsub st0,st1        
        fld qword [divisor]
        fdivr st0,st1
        fstp qword [resultNeg]      ;store negative result

        ret
