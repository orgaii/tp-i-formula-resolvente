#!/bin/bash
#------------------------------------------------------
# PALETA DE COLORES
#------------------------------------------------------
#setaf para color de letras/setab: color de fondo
	black=$(tput setaf 0);
	bg_red=$(tput setab 1);
	reset=$(tput sgr0);
	bold=$(tput setaf bold);

#------------------------------------------------------
# DIRECTORIO DE TRABAJO
#------------------------------------------------------

proyectoActual=$(pwd);

#------------------------------------------------------
# DISPLAY MENU
#------------------------------------------------------

imprimir_menu () {
    imprimir_encabezado "  __  __                    _______ _____    __  \n\t\t |  \/  |                  |__   __|  __ \  /_ | \n\t\t | \  / | ___ _ __  _   _     | |  | |__) |  | | \n\t\t | |\/| |/ _ \ '_ \| | | |    | |  |  ___/   | | \n\t\t | |  | |  __/ | | | |_| |    | |  | |       | | \n\t\t |_|__|_|\___|_| |_|\__,_|    |_|  |_|__ ____|_| \n\t\t   / __ \|  __ \ / ____|   /\     |_   _|_   _|  \n\t\t  | |  | | |__) | |  __   /  \      | |   | |    \n\t\t  | |  | |  _  /| | |_ | / /\ \     | |   | |    \n\t\t  | |__| | | \ \| |__| |/ ____ \   _| |_ _| |_   \n\t\t   \____/|_|  \_\\______/_/    \_\ |_____|_____|  \n\t\t                                                 ";
    echo -e "\t\t El directorio de trabajo es:";
    echo -e "\t\t $proyectoActual";

    echo -e "\t\t";
    echo -e "\t\t Opciones:";
    echo "";
    echo -e "\t\t\t a.  Compilar programa resolvente ecuación cuadrática";
    echo -e "\t\t\t b.  Compilar programa producto escalar del vector";
    echo -e "\t\t\t c.  Ejecutar programa resolvente ecuación cuadrática";
    echo -e "\t\t\t d.  Ejecutar programa producto escalar del vector";
    echo -e "\t\t\t e.  Ver código resolvente cuadraticas";
    echo -e "\t\t\t f.  Ver código producto escalar del vector";
    
    echo -e "\t\t\t q.  Salir";
    echo "";
    echo -e "Escriba la opción y presione ENTER";
}

#------------------------------------------------------
# FUNCTIONES AUXILIARES
#------------------------------------------------------

imprimir_encabezado () {
    clear;
    #Se le agrega formato a la fecha que muestra
    #Se agrega variable $USER para ver que usuario está ejecutando
    echo -e "`date +"%d-%m-%Y %T" `\t\t\t\t USERNAME:$USER";
    echo "";
    #Se agregan colores a encabezado
    echo -e "\t\t${bg_red}${black}${bold}-------------------------------------------------\t${reset}";
    echo -e "\t\t${bold}${bg_red}${black}$1\t\t${reset}";
    echo -e "\t\t${bg_red}${black}${bold}-------------------------------------------------\t${reset}";
    echo "";
}

esperar () {
    echo "";
    echo -e "Presione enter para continuar";
    read ENTER ;
}

malaEleccion () {
    echo -e "Selección Inválida ..." ;
}

decidir () {
	echo $1;
	while true; do
		echo "desea ejecutar? (s/n)";
    		read respuesta;
    		case $respuesta in
        		[Nn]* ) break;;
       			[Ss]* ) eval $1
				break;;
        		* ) echo "Por favor tipear S/s ó N/n.";;
    		esac
	done
}

#------------------------------------------------------
# FUNCTIONES del MENU
#------------------------------------------------------

a_funcion () {
  	imprimir_encabezado " Opción a.                                       \n\t\t Compilar programa resolvente ecuación cuadrática";
    decidir "cd $proyectoActual/formula_resolvente; nasm -f elf resolvente.asm -o resolvente.o; gcc -m32 resolvente.c resolvente.o -o ../resolvente";
}

b_funcion () {
   	imprimir_encabezado " Opción b.                                       \n\t\t Compilar programa producto escalar del vector   ";
    decidir "cd $proyectoActual/escalar_vector; nasm -f elf escalarVector.asm -o escalarVector.o; gcc -m32 escalarVector.o -o ../escalarVector";

}

c_funcion () {
   	imprimir_encabezado " Opción c.                                       \n\t\t Ejecutar programa resolvente ecuación cuadrática";
   	echo -e "Ingrese el valor del parametro 'a'"
    read a;
    echo -e "Ingrese el valor del parametro 'b'"
    read b;
    echo -e "Ingrese el valor del parametro 'c'"
    read c;
    decidir "cd $proyectoActual; ./resolvente $a $b $c";
}

d_funcion () {
    imprimir_encabezado " Opción d.                                       \n\t\t Ejecutar programa producto escalar del vector   ";
    decidir "cd $proyectoActual; ./escalarVector";
}

e_funcion () {
    imprimir_encabezado " Opción e.                                       \n\t\t Ver código resolvente cuadraticas               ";
    decidir "less $proyectoActual/formula_resolvente/resolvente.c; less $proyectoActual/formula_resolvente/resolvente.asm"
}

f_funcion () {
	imprimir_encabezado " Opción f.                                       \n\t\t Ver código producto escalar del vector          ";
    decidir "less $proyectoActual/escalar_vector/escalarVector.asm"
}

q_funcion () {
	clear;
	exit;
}

#------------------------------------------------------
# LOGICA PRINCIPAL
#------------------------------------------------------

while  true
do
    # 1. mostrar el menu
    imprimir_menu;
    # 2. leer la opcion del usuario
    read opcion;

    case $opcion in
        a|A) a_funcion;;
        b|B) b_funcion;;
        c|C) c_funcion;;
        d|D) d_funcion;;
        e|E) e_funcion;;
        f|F) f_funcion;;
        q|Q) q_funcion;;

        *) malaEleccion;;
    esac
    esperar;
done
