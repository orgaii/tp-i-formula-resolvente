<div align="center">-

# Organización del computador II – UNGS 2do cuatrimestre del 2021 
<img src="/img/duck.gif" width="180" height="180"/>

## TP I - Resolvente en ASM
**Daniel Carrillo**
</div>

---

# Tabla de Contenidos
1. [Clonar y ejecutar el programa.](#titulo1)
2. [Como usar el menú de ejecución.](#titulo2)
3. [Problemas, decisiones y evolución en el código.](#titulo3)
    1. [Formula resolvente.](#subtitulo3.1)
    2. [Producto escalar de un vector.](#subtitulo3.2)
4. [Capturas de funcionamiento de los programas.](#titulo4)
    1. [Formula resolvente.](#subtitulo4.1)
    2. [Producto escalar de un vector.](#subtitulo4.2)
5. [Observaciones generales.](#titulo5)
6. [Conclusión.](#titulo6)
---

## Clonar y ejecutar el programa:<a name="titulo1"></a>

Para clonar el programa ejectucar erl siguiente comando:

### `git clone https://gitlab.com/orgaii/tp-i-formula-resolvente.git`

Para iniciar el programa ejecutar el escript de bash de la siguente manera:

### `./init_menu.sh`

---
## Como usar el menú de ejecución:<a name="titulo2"></a>
<div align="center">
<img src="/img/menu.jpeg" width="700" height="500"/>
</div>
_a.  Compilar programa resolvente ecuación cuadrática_ <br />
_b.  Compilar programa producto escalar del vector_ <br /><br />
Elegir cualquiera de estas opciones para generar el archivo objeto y el ejecutable del programa seleccionado. Es necesario primero compilar para luego ejecutar.
_c.  Ejecutar programa resolvente ecuación cuadrática_ <br />
_d.  Ejecutar programa producto escalar del vector_ <br /><br />
Elegir cualquiera de estas opciones para ejecutar el programa seleccionado.
_e.  Ver código resolvente cuadraticas_ <br />
_f.  Ver código producto escalar del vector_ <br /><br />
Elegir cualquiera de estas opciones para ver el código del programa seleccionado con el comando less.

---
## Problemas, decisiones y evolución en el código:<a name="titulo3"></a>

### Formula resolvente <a name="subtitulo3.1"></a>

Tomando la primera versión del código:

```
extern printf
section .data

a dq 1.0
b dq 6.0
c dq 8.0

notb        dq 0.0
squaredb    dq 0.0

cons4       dq 4.0
cons2       dq 2.0

numbering   dq 0.0
dividend    dq 0.0

resultPos   dq 0.0
resultNeg   dq 0.0

format1 db "Las raíces son: %f y %f" , 10,13,0

section .text
global CMAIN

CMAIN:
    mov ebp, esp; for correct debugging

    fld     qword [b]           ;load b in fpu 
    fchs                        ;change sign
    fstp     qword [notb]       ;store -b in notb
    
    fld     qword [cons2]       ;load cons 2 in fpu
    fld     qword [a]           ;load a in fpu  
    fmul    st0, st1
    fstp     qword [dividend]
    
    fld     qword [b]           ;load b in fpu
    fmul    st0, st0            ;b squared
    fstp    qword [squaredb]    ;store b^2 in squaredb
    
    fld     qword [cons4]       ;load cons 4 in fpu
    fld     qword [a]           ;load a in fpu
    fld     qword [c]           ;load c in fpu
    fmul    st0, st1
    fmul    st0, st2
    fld     qword [squaredb]
    fsub    st0, st1
    fsqrt
    fst     qword [numbering]   ;store root square b^2 - 4*a*c in numbering
        
    fld     qword [notb]       ;load notb in fpu
    fadd    st0,st1
    
    fld     qword [dividend]
    fdivr   st0,st1
    fstp    qword [resultPos] 
    finit  
    
    fld     qword [numbering]
    fld     qword [notb]       ;load notb in fpu
    fsub    st0,st1
    
    fld     qword [dividend]
    fdivr   st0,st1
    fstp    qword [resultNeg] 
  

    push dword [resultNeg+4]
    push dword [resultNeg]
    push dword [resultPos+4]
    push dword [resultPos]
    push format1
    call printf
    add esp, 20

    xor eax, eax
    ret

```

Se puede notar que no se crearon subrutinas y se trabajo directamente sobre la FPU, esto genero problemas los cuáles en ese momento se utilizó la instrucción: `finit` para inicializar la FPU antes que se desborde la pila.<br />

En la segunda y útima versión se crearon subrutinas para entender y manejar mejor el código como se puede observar:

```
resolute:
    push ebp
    mov ebp, esp                   ;enter 0, 0

    call extract_parameters    
    call print_variables
    call store_neg_b
    call store_divisor
    call store_b_square
    call store_numerator
    call calculate_result_pos
    call calculate_result_neg

    push dword [resultNeg+4]
    push dword [resultNeg]         ;push negative result to print
    push dword [resultPos+4]
    push dword [resultPos]         ;push positive result to print
    push format1
    call printf
    add esp, 20

    xor eax, eax
    mov ebp,esp
    pop ebp                        ;leave  

    ret
```

### Producto escalar de un vector: <a name="subtitulo3.2"></a>

Tomando la primera versión del código:

```
extern _printf
extern printf

section .data

puntero dq 25.0,0.0,1.0,0.0
resul dq 0.0
formato db "el numero es : %f", 10,13,0
vector dq 25.0,0.0,1.0,0.0
length dd 4

msg db "El vector es:", 10,13,"[",0
format db " %f ,",0
end db " %f ]",0
n dq 2.0


CMAIN:
    mov ebp, esp; for correct debugging
    
    push puntero
    push vector
    push dword[length]
    mov edx, [n+4]
    push edx
    mov edx, [n]
    push edx
    call producto_rvf
    add esp, 12
    push dword[resul+4]
    push dword[resul] 
    push formato
    add esp, 16
    push msg
    call printf
    add esp, 12
    add esp, 4
    push vector
    push dword[length]
    call loop_print
    add esp, 8
    xor eax, eax
    ret
    
    producto_rvf:
        push ebp
        mov ebp, esp
        fld qword [ebp+8]
        mov eax, [ebp+16]
        mov ebx, [ebp+20]
        _loop:
            cmp eax, 0
            jz _leave
            dec eax
            fld qword[ebx]
            fmul st0, st1
            fstp qword[ebx]
            add ebx, 8
            jmp _loop        
        
        _leave:
            mov ebp,esp
            pop ebp    
     
            ret
            
     loop_print:
        push ebp
        mov ebp, esp
        mov eax, [ebp+8]
        mov ebx, [ebp+12]
        _loop_:
            cmp eax, 0
            jz _leave_
            cmp eax, 1
            jz last
            dec eax
            push eax
            push dword[ebx+4]
            push dword[ebx]
            push format
            call printf
            add esp, 12
            pop eax
            add ebx, 8
            
            jmp _loop_ 
            
        last:   
            dec eax
            push eax
            push dword[ebx+4]
            push dword[ebx]
            push end
            call printf
            add esp, 12
            pop eax
            add ebx, 8
            
            jmp _loop_    
        
        _leave_:
            mov ebp,esp
            pop ebp    
     
            ret
```
En una primera versión se genera un código casi terminada se opta por dos subrutinas importantes, una para multiplicar los elementos del vector y otra para imprimir en orden el resultado.
En una segunda y última versión se corrigen algunos aspectos menores a la vez que se agregan comentarios para una mejor lectura.

## Capturas de funcionamiento de los programas:<a name="titulo4"></a>

### Formula resolvente <a name="subtitulo4.1"></a>
<div align="center">
<img src="/img/resol_cuadratica.png"/>
</div>

### Producto escalar de un vector: <a name="subtitulo4.2"></a>

<div align="center">
<img src="/img/resol_escalar.png"/>
</div>

## Observaciones generales:<a name="titulo5"></a>

* Los programas desarrollados estan pensados para ser ejecutado en un sistema operativo con una distribución de Linux por lo que se recomienda utilizar.
* Para realizar el script de ejecución del trabajo en bash. Se opto por reutilizar la base de un menú realizado anteriormente para un trabajo de SOR I `code reuse`
* Para la realización del documento README.md en principio se opto por una herramienta de conversión pero finalmente resulto más sensillo realizarlo con algunas búsquedas y un poco de documentación.

## Conclusión:<a name="titulo6"></a>
<div align="center">
<img src="/img/homer.gif"/>
</div>
